let cartHelper = require("../helpers/Cart-helpers"); 
// var db=requir('../config/connection')
let express = require('express');
let router = express.Router();


/* GET users listing. */
router.get('/getCartProduct', function(req, res, next) {
  cartHelper.getProductFromCart(result=>{
    res.send(result);
  });
});

router.delete('/getCartProduct', function(req, res, next) {
  let id = req.query.id
  cartHelper.deleteProductFromCart(id,(result)=>{

    res.send(result);
  })
});

router.put('/getCartProduct', function(req, res, next) {
  let id = req.query.id
  let qty = req.query.qty
  cartHelper.updateQuantity(id,qty,(result)=>{

    res.send(result);
  })
});
router.post('/getCartProduct',function(req,res,next){
  let {productImages,productName,quantity,company,price,offer} = req.body;
  let prod={
    productImages:productImages,
    productName:productName,
    quantity:quantity,
    company:company,
    price:price,
    offer:offer,
    
  }
  console.log("product = ",prod);
  cartHelper.addProduct(prod,(result)=>{

      res.send(result);
    })
});
router.get('/applyCoupens',function(req,res,next){

  let id = req.query.id

  cartHelper.applyCoupon(id,(result)=>{
      res.send(result);
    })
});

router.get('/homePage',function(req,res,next){
  res.send('HOME')
});
router.get('/checkOut',function(req,res,next){

  
  cartHelper.checkOut((result)=>{
      res.send(result);
    })
});






module.exports = router;
