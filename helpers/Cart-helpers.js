var db=require('../config/connection')
const {ObjectId, ResumeToken} = require('mongodb');
const { query } = require('express');


    module.exports.getProductFromCart = function(callback){
        db.get().collection('Cart').find().toArray().then((data)=>{
            // console.log(data);
            callback(data);
        })

    };
    module.exports.deleteProductFromCart = function(id,callback){
        let query = {_id: ObjectId(id)};
        db.get().collection('Cart').deleteOne(query).then((data)=>{
            console.log(data.result.ok);
            callback(data);
        })
        
    };
    module.exports.updateQuantity= function(id,qty,callback){
        let query = {_id: ObjectId(id)};

        db.get().collection('Cart').findOneAndUpdate(query,{$set:{quantity:qty}}).then((data)=>{
            console.log(data);
            callback(data);
        })   
    };
    module.exports.addProduct = function(products,callback){        

        db.get().collection('Cart').insertOne(products).then((data)=>{
            callback(data.result)
            console.log(data.result)
        })
    };
    module.exports.applyCoupon = function(id,callback){     
        let query ={_id: ObjectId(id)};      

        db.get().collection('Cart').find(query).toArray().then((data)=>{ 

        
           
            let price = data[0].price;
            let offer = data[0].offer;
           

            discount = offer*price/100
            

            coupen = price-discount

            console.log(coupen);
            
            db.get().collection('Cart').findOneAndUpdate(query,{$set:{offerPrice:coupen}},{upsert:true}).then((data)=>{
                callback(data)
                console.log(data);
               
            })
        
        })
       
    };
    module.exports.checkOut = function(callback){   
            

  let total = db.get().collection('Cart').aggregate([
          
            { 
                $group: {
                    _id : null, 
                    sum : { 
                        $sum: "$price" 
                    },
                    count : { 
                        $sum:1 
                    },
                    offerPrice : { 
                        $sum: "$offerPrice" 
                    },
                } 
            }
        ]).toArray().then(result=>{

            console.log(result);

            let data = {
                totalSum : result[0].sum,
                discountAmount : result[0].sum-result[0].offerPrice,
                productCount : result[0].count
            }
            callback(data);
        })
    };



